﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElseIf
{
    class Program
    {
        static void Main(string[] args)
        {
            // küsi kasutajalt arv1
            // küsi kasutajalt arv2
            // kui kaks arvu on võrdsed, prindi tekst arvud on võrdsed
            // kui esimene arv on suurem kui teine, prindi, et esimene on suurem
            // muul juhul prindi, et teine on suurem

            Console.WriteLine("Sisesta arv");

            int firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Sisesta veel üks arv");

            int secondNumber = Convert.ToInt32(Console.ReadLine());

            if (firstNumber == 0 && secondNumber == 0)
            {
                Console.WriteLine("Mõlemad arvud on 0");
            }

            else if (firstNumber != 0 && secondNumber != 0 && firstNumber == secondNumber)
            {
                Console.WriteLine("Arvud on võrdsed");
            }

            else if (firstNumber > secondNumber)
            {
                Console.WriteLine("Esimene arv on suurem");
            }

            // NB! else kehtib ainult eelneva if'i kohta
            else
            {
                Console.WriteLine("Teine arv on suurem");
            }
                
            Console.ReadLine();
        }
    }
}
