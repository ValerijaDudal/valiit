﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class WildAnimal: Animal
    {
        public string TypeOfNature { get; set; }

        public virtual void HideFromDanger()
        {
            Console.WriteLine("Ohtlik! Peidan ennast ära");
        }

    }
}
