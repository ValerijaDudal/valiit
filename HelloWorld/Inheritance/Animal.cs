﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Animal
    {
        public string Breed { get; set; }
        public virtual string Name { get; set; }
        public string Color { get; set; }
        public int Age { get; set; }

        // virtual tähendab, et seda meetodit saavad pärinevad klassid üle kirjutada
        public virtual void Eat()
        {
            Console.WriteLine("Söön toitu");
        }

        public virtual void PrintInfo()
        {
            Console.WriteLine("{0} Info: ", GetType().Name);
            Console.WriteLine("Nimi on {0}", Name);
            Console.WriteLine("Tõug on {0}", Breed);
            Console.WriteLine("Vanus on {0}", Age);
            Console.WriteLine("Värv on {0}", Color);

        }

        public string GetInfo()
        {
            return String.Format("Tüüp on {0}, nimi on {1}, tõug on {2}", GetType().Name, Name, Breed);
        }
    }
}
