﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Dog : Pet
    {
        public bool IsChained { get; set; }
        private int yearOfBirth;

        // Parameetriteta konstruktor
        // Vaikimisi on alati igal klassil üks tühi parameetriteta konstruktor senikaua kui me uue konstruktori loome.
        // Kui ma loon näiteks uue 2 parameetriga konstruktori, siis kustutatakse vaikimisi nähtamatu parameetritega konstruktor ära
        public Dog()
        {
            Console.WriteLine("Loodi Dog objekt");
            // Vaikimisi kõik koerad on Matid
            Name = "Mati";

            yearOfBirth = DateTime.Now.Year;
            // Vaikimisi kõik koerad on 1 aastased
            Age = 1;
            // Vaikimisi kõik koerad on ketis
            IsChained = true;
        }

        public Dog(string name, int age)
        {
            Name = name;
            Age = age;
        }


        public void Bark(string language)
        {
            if(language == "ru")
            {
                Console.WriteLine("Gav gav");
            }
            else if (language == "eng")
            {
                Console.WriteLine("Wof wof");
            }
            else
            {
                Console.WriteLine("Auf auf");
            }
        }

        public override void PrintInfo()
        {
            base.PrintInfo();
            Console.WriteLine("Koer on ketil", IsChained);
        }
    }
}
