﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class DomesticAnimal : Animal
    {
        public string OwnerName { get; set; }

        public void LookForMaster()
        {
            Console.WriteLine("Kus on minu omanik?");
        }

        public override void PrintInfo()
        {
            Console.WriteLine("Omaniku nimi on {0}", OwnerName);
            base.PrintInfo();     
        }

        public override void Eat()
        {
            base.Eat();
        }
    }
}
