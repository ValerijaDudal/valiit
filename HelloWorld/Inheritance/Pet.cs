﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Pet : DomesticAnimal
    {
        public string FavoriteToy { get; set; }

        public void AskForSnack()
        {
            Console.WriteLine("Anna palun snäkk");
        }

        public override void PrintInfo()
        {
            base.PrintInfo();
            if(FavoriteToy != null)
            {
                Console.WriteLine("Lemmik mänguasi on {0}", FavoriteToy);
            }    
        }
    }
}
