﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Pärinemine
// Põlvnemine
namespace Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat persian = new Cat();
            persian.Age = 2;
            persian.Name = "Miisu";
            persian.Breed = "Persian";
            persian.Color = "Grey";


            persian.PrintInfo();

            persian.CatchMouse();
            persian.Eat();

            Console.WriteLine();



            Cat angora = new Cat()
            {
                Name = "Kitu",
                Age = 3,
                Breed = "Angora",
                Color = "White",
                LivesRemaining = 8,
                FavoriteToy = "Pall",
                OwnerName = "Kalle"

            };

            angora.PrintInfo();

            angora.CatchMouse();
            angora.Eat();

            Console.WriteLine();



            Dog buldog = new Dog()
            {
                Name = "Naks",
                Age = 10,
                Breed = "Buldog",
                Color = "Black",
                IsChained = true
            };

            buldog.PrintInfo();

            Console.WriteLine();

            buldog.Eat();
            buldog.Bark("est");

            Hare hare = new Hare();
            hare.Name = "Juku";

            Wolf wolf = new Wolf();
            wolf.Hunt(hare);

            Console.WriteLine(wolf.ToString());

            wolf.Eat();
            hare.Eat();
            angora.Eat();

            Console.WriteLine();

            angora.PrintInfo();

            Console.WriteLine();

            buldog.PrintInfo();

            Console.WriteLine();

            Deer deer = new Deer()
            {
                Name = "Rudolf",
                Age = 8,
                Breed = "North",
                Color = "Brown",
                Height = 165
            };

            deer.PrintInfo();

            Console.WriteLine();

            wolf.HideFromDanger();

            Console.WriteLine();

            Fox fox = new Fox();
            fox.HideFromDanger();

            Console.ReadLine();
        }
    }
}
