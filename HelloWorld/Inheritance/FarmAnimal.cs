﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class FarmAnimal : DomesticAnimal
    {
        public string Usage { get; set; }

        public void GoToShelter()
        {
            Console.WriteLine("Lähe aita");
        }

        public override void Eat()
        {
            base.Eat();
        }
    }
}
