﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Cat : Pet
    {
        public int LivesRemaining { get; set; }

        public Cat()
        {
            
        }

        public Cat(int age)
        {
            Age = age;
        }

        public void CatchMouse()
        {
            Console.WriteLine("Püüan hiiri");
        }

        public override void PrintInfo()
        {
            base.PrintInfo();
            Console.WriteLine("Elude arv on {0}", LivesRemaining);
        }

        public override string Name
        {
            get
            {
                return "dr. " + base.Name;
            }
        }
    }
}
