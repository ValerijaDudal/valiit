﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Deer : Herbivore
    {
        public int Height { get; set; }

        public void SleepUnderTree()
        {

        }

        public override void PrintInfo()
        {
            base.PrintInfo();
            Console.WriteLine("Põdra pikkus on {0}", Height);
        }
    }
}
