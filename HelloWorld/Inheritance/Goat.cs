﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Goat : FarmAnimal
    {
        public override void Eat()
        {
            // Lähim klass, kus selline meetod on olemas või üle kirjutatud
            base.Eat();
        }

        public override void PrintInfo()
        {
            base.PrintInfo();
        }
    }
}
