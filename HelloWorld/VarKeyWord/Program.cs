﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VarKeyWord
{
    class Program
    {
        enum Gender { Male, Female}

        static void Main(string[] args)
        {
            // var määrab ise tüübi
            // Täisarvu puhul pannakse väärtuseks int või kui ei mahu pannakse uint ja kui ikka ei mahu pannakse long
            var b = 3; // int
            int a = 3; // int
            var c = 3.0; // double
            var d = 3000000000; // uint
            var e = 5000000000; // long
            var f = 1000000000000000000; // ulong

            Console.WriteLine(a.GetType());
            Console.WriteLine(b.GetType());
            Console.WriteLine(c.GetType());

            var g = "3"; // string
            var h = '3'; // char
            var i = 3.0f; // float
            var j = 3.0d; // double
            var k = 3.0m; // decimal

            var numbers = new[] { 1, 2, 3 };
            var words = new[] { "1", "2", "3" };
            var doubles = new int[3];

            var fileStream = new FileStream("minuFail.txt", FileMode.Append, FileAccess.Write);

            var gender = Gender.Female;

            Console.ReadLine();
        }
    }
}
