﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integers
{
    class Program
    {
        static void Main(string[] args)
        {
            int number;

            number = 3;

            Console.WriteLine(number);

            int a = 4;
            int b = -7;

            // Kaks arvu on 4 ja -7
            Console.WriteLine("Kaks arvu on " + a + " ja " + b);
            Console.WriteLine("Kaks arvu on {0} ja {1}", a, b);

            Console.WriteLine();

            // string + string = string
            // string + int = string
            // stringi ja täisarvu liitmisel teisendakse täisarv stringiks ja siis liidetakse
            // int + int = int
            // matemaatiline arvude liitmine

            // Arvude 4 ja -7 summa on -3
            Console.WriteLine("Arvude " + a + " ja " + b + " summa on " + (a + b));
            Console.WriteLine("Arvude {0} ja {1} summa on {2}", a, b, a + b);

            Console.WriteLine();

            // Arvude 4 ja -7 vahe on 11
            Console.WriteLine("Arvude " + a + " ja " + b + " vahe on " + (a - b));
            Console.WriteLine("Arvude {0} ja {1} vahe on {2}", a, b, a - b);

            Console.WriteLine();
            
            // Arvude 4 ja -7 korrutis on -28
            Console.WriteLine("Arvude " + a + " ja " + b + " korrutis on " + (a * b));
            Console.WriteLine("Arvude {0} ja {1} korrutis on {2}", a, b, a * b);

            Console.WriteLine();

            // Arvude 4 ja -7 jagatis on 0
            Console.WriteLine("Arvude " + a + " ja " + b + " jagatis on " + (a / b));
            Console.WriteLine("Arvude {0} ja {1} jagatis on {2}", a, b, a / (float)b);

            Console.WriteLine();

            // Arvude -7 ja 4 jagatis on -1.75
            Console.WriteLine("Arvude " + b + " ja " + a + " jagatis on " + (b / a));
            Console.WriteLine("Arvude {0} ja {1} jagatis on {2}", b, a, b / (double)a);

            // Kahe täisarvu jagamisel tulemus on täisarv, kus kõik peale koma süüakse ära
            // NB! Ei toimu ümardamist

            Console.WriteLine();

            // 2147483647 + 1

            int c = 2147483647;
            int d = 1;

            Console.WriteLine(c + d);

            Console.ReadLine();
        }
    }
}
