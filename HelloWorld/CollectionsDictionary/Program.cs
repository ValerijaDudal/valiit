﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            //// "Auto" => "Car"
            //// "Maja" => "House"
            //// "Kass" => "Cat"
            //// "Koer" => "Dog"

            //// Sõnaraamat ehk igal elemendil on oma tähendus
            //Dictionary<string, string> dictionaryEn = new Dictionary<string, string>();

            //dictionaryEn.Add("auto", "Car");
            //dictionaryEn.Add("maja", "House");
            //dictionaryEn.Add("kass", "Cat");
            //dictionaryEn.Add("koer", "Dog");


            //Console.WriteLine("Keys:");
            //foreach (var item in dictionaryEn.Keys)
            //{
            //    Console.WriteLine(item);
            //}

            //Console.WriteLine();


            //Console.WriteLine("Values:");
            //foreach (var item in dictionaryEn.Values)
            //{
            //    Console.WriteLine(item);
            //}

            //Console.WriteLine();

            

            //Console.WriteLine("Maja inglise keeles on {0}", dictionaryEn["maja"]);
            //Console.WriteLine("Maja inglise keeles on {0}", dictionaryEn["kass"]);




            //Dictionary<string, string> dictionaryDe = new Dictionary<string, string>();
            //dictionaryDe.Add("auto", "Auto");
            //dictionaryDe.Add("maja", "Haus");
            //dictionaryDe.Add("koer", "Hund");
            //dictionaryDe.Add("kass", "Katze");

            //Console.WriteLine("Mis keelde soovid sõna tõlkida? inglise/saksa");
            //string language = Console.ReadLine();
            //Console.WriteLine("Mis sõna soovid tõlkida?");
            //Console.WriteLine("Tunnen selliseid sõnu: {0}", string.Join(", ", dictionaryEn.Keys));
            //string word = Console.ReadLine().ToLower();

            //if(language == "inglise")
            //{
            //    Console.WriteLine("Sõna {0} {1} keeles on {2}", word, language, dictionaryEn[word]);
            //}

            //else
            //{
            //    Console.WriteLine("Sõna {0} {1} keeles on {2}", word, language, dictionaryDe[word]);
            //}



            Dictionary<string, string> countryCodes = new Dictionary<string, string>();
            countryCodes.Add("EE", "Estonia");
            countryCodes.Add("GER", "Germany");
            countryCodes.Add("USA", "United States of America");

            // Asenda United States of America => United States

            // Value asendamine
            countryCodes["USA"] = "United States";






            // Küsi kasutajalt riigikood ja sellele vastav riigi nimetus
            // Kui selline riigikood on olemas, siis asenda riik
            // Aga kui sellist riigikoodi veel ei olnud, siis lisa

            Console.WriteLine("Palun sisesta riigikood");
            string answerCode = Console.ReadLine();
            Console.WriteLine("Mis riigile see vastab?");
            string answerCountry = Console.ReadLine();

            if (countryCodes.ContainsKey(answerCode))
            {
                countryCodes[answerCode] = answerCountry;
            }
        
            else
            {
                
                countryCodes.Add(answerCode, answerCountry);
                
            }

            Console.WriteLine("{0} - {1}", answerCode, answerCountry);

            Console.ReadLine();

            // Näitab kõik elemendid
            foreach (var item in countryCodes)
            {
                Console.WriteLine("{0} => {1}", item.Key, item.Value);
            }

            Console.ReadLine();
        }
    }
}
