﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountWords
{
    class Program
    {
        static void Main(string[] args)
        {
            // Küsi kasutajalt lause ja loe kokku kõik erinevad sõnad ja prindi välja, mitu korda need sõnad esinevad

            // "Mina ja Pets läksime tööle ja siis koju ja siis tööle jälle"
            // Mina 1x
            // ja 3x
            // Pets 1x
            // läksime 1x
            // tööle 2x
            // siis 2x
            // koju 1x
            // jälle 1x

            Dictionary<string, int> wordCounts = new Dictionary<string, int>();

            Console.WriteLine("Kirjuta lause");
            string sentence = Console.ReadLine();

            string[] words = sentence.Split(' ');

            foreach (var word in words)
            {
                if(!wordCounts.ContainsKey(word))
                {
                    wordCounts.Add(word, 1);
                }
                else
                {
                    wordCounts[word] = wordCounts[word] + 1;
                }
            }

            foreach (var item in wordCounts)
            {
                Console.WriteLine("{0} {1}", item.Key, item.Value);
            }

            Console.ReadLine();
        }
    }
}
