﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IfElse
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Sisesta oma nimi");

            string name = Console.ReadLine();
            string myName = "Valerija";

            // valerija -> VALERIJA
            // VALERIJA -> VALERIJA

            // if(name.Equals(myName, StringComparison.InvariantCultureIgnoreCase)) - siia saab tingimusi lisada

            // pole vahet, kas sisestad suure või väikse tähe
            if (name.ToUpper() == myName.ToUpper())
            {
                Console.WriteLine("Tere, " + myName + "!");
                Console.WriteLine("Kui vana sa oled?");

                int a = Convert.ToInt32(Console.ReadLine());

                if (a >= 18)
                {
                    Console.WriteLine("Tere tulemast klubisse!");
                }

                else
                {
                    Console.WriteLine("Sisse ei pääse");
                }
          
            }

            else
            {
                Console.WriteLine("Ma ei tunne sind. Aga mis on su perekonnanimi?");

                string lastName = Console.ReadLine();

                if (lastName == "Dudal" || lastName == "dudal")
                {
                    Console.WriteLine("Tere " + name + " " + lastName + "!");
                }

                else
                {
                    Console.WriteLine("Ma ikka sind ei tunne " + name + " " + lastName + "!");
                }
            }

            
            Console.ReadLine();
        }
    }
}
