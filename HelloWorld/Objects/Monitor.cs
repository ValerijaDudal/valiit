﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    // Enum on tüüp, kus saab defineerida erinevad valikud ja kasutatakse siis, kui valikud ei muutu programmi töötamise jooksul
    // Tegelikult salvestatakse enumid alati int'ina
    enum ScreenType
    {
        LCD, TFT, OLED, AMOLED 
    }

    enum Color
    {
        Blue = 1, Grey = 2, Black = 3, White = 4, Red = 5
    }

    enum ScreenSize
    {
        Large = 65, Medium = 32, Small = 22
    }

    class Monitor
    {
        // privaat muutujad
        // field
        private Color color;
        // inch
        private double diagonal = 0;
        private string manufacturer;
        private ScreenType screenType;
        private ScreenSize screenSize;



        // Property
        // get ja set meetodeid nimetatakse accessors
        public string Manufacturer
        {
            get
            {
                if (manufacturer == "Tootja puudub")
                {
                    Console.WriteLine("Kahjuks on sellel monitoril tootja seadistamata");
                    Console.WriteLine("Palun kirjuta tootja");
                    manufacturer = Console.ReadLine();
                }
                return manufacturer;
            }
            set
            {
                if (value == "Huawei")
                {
                    Console.WriteLine("Sellise tootja monitore meil pole");
                    manufacturer = "Tootja puudub";
                }
                else
                {
                    // value tähendab seda väärtust, mis parasjagu panen property väärtuseks
                    manufacturer = value;
                }

            }
        }




        public ScreenType ScreenType
        {
            get
            {
                return screenType;
            }
            set
            {
                screenType = value;
            }
        }






        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }





        public ScreenSize ScreenSize
        {
            get
            {
                return screenSize;
            }
            set
            {
                screenSize = value;

                if (value == ScreenSize.Small)
                {
                    diagonal = 22;
                }
                else if (value == ScreenSize.Medium)
                {
                    diagonal = 32;
                }
                else
                {
                    diagonal = 65;
                }

            }
        }






        public double Diagonal
        {
            get
            {
                return diagonal;
            }
            //    set
            //    {
            //        diagonal = value;
            //    }
            //}



            // Kapseldamine tähendab, et klass kontrollib, millistele muutujatele lubab väljaspoolt ligi pääseda (teised klassid)
            // Encapsulation








        }
    }
}
