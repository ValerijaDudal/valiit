﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    class Program
    {
        static void Main(string[] args)
        {
            // Loon klassist Monitor ühe eksemplari ehk objekti nimega monitor

            Monitor lgMonitor = new Monitor();
            lgMonitor.Manufacturer = "Huawei";

            Console.WriteLine("Monitori tootja on {0}", lgMonitor.Manufacturer);

            lgMonitor.Manufacturer = "LG";
            lgMonitor.Color = Color.Black;
            lgMonitor.ScreenType = ScreenType.OLED;
            lgMonitor.ScreenSize = ScreenSize.Large;

            Console.WriteLine("Monitori tootja on {0}, värv on {1}, diagonaal on {2}, ekraani tüüp on {3} ja ekraani suurus on {4}",
                lgMonitor.Manufacturer, lgMonitor.Color, lgMonitor.Diagonal, lgMonitor.ScreenType, lgMonitor.ScreenSize);

            // Keela kasutajal diagonaali seadistamine, luba ainult küsida
            // Kui kasutaja seadistab ekraani suuruse, siis seadista automaatselt selle järgi ka diagonaal

            int diagonal = (int)ScreenSize.Large;
            ScreenSize screenSize = (ScreenSize)65;
            Console.WriteLine(screenSize);

            Monitor sonyMonitor = new Monitor();
            sonyMonitor.Manufacturer = "Sony";
            sonyMonitor.Color = Color.Blue;
            sonyMonitor.ScreenType = ScreenType.AMOLED;
            sonyMonitor.ScreenSize = ScreenSize.Medium;


            Console.WriteLine("Monitori tootja on {0}, värv on {1}, diagonaal on {2}, ekraani tüüp on {3} ja ekraani suurus on {4}",
                sonyMonitor.Manufacturer, sonyMonitor.Color, sonyMonitor.Diagonal, sonyMonitor.ScreenType, sonyMonitor.ScreenSize);

            Monitor hpMonitor = new Monitor();
            hpMonitor.Manufacturer = "HP";
            hpMonitor.Color = Color.Red;
            hpMonitor.ScreenType = ScreenType.LCD;
            hpMonitor.ScreenSize = ScreenSize.Small;

            Console.WriteLine("Monitori tootja on {0}, värv on {1}, diagonaal on {2}, ekraani tüüp on {3} ja ekraani suurus on {4}",
                hpMonitor.Manufacturer, hpMonitor.Color, hpMonitor.Diagonal, hpMonitor.ScreenType, hpMonitor.ScreenSize);

            Console.WriteLine();

            // Tee massiiv 3 monitorist
            // Prindi välja kõikide monitoride värvid
            // Prindi välja kõigi nende monitore tootjad, mille diagonaal on suurem kui 22"

            Monitor[] allMonitors = new Monitor[] { lgMonitor, sonyMonitor, hpMonitor };

            for (int i = 0; i < allMonitors.Length; i++)
            {
                Console.WriteLine(allMonitors[i].Color);
            }

            Console.WriteLine();

            for (int i = 0; i < allMonitors.Length; i++)
            {
                if(allMonitors[i].Diagonal > 22)
                {
                    Console.WriteLine(allMonitors[i].Manufacturer);
                }
            }

            Console.WriteLine();

            foreach (Monitor monitor in allMonitors)
            {
                Console.WriteLine(monitor.Color);
            }

            Console.WriteLine();

            foreach (Monitor monitor in allMonitors)
            {
                if (monitor.Diagonal > 22)
                {
                    Console.WriteLine(monitor.Manufacturer);
                }
            }
            Console.ReadLine();
        }
    }
}
