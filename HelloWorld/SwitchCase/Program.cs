﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchCase
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("a)");
            Console.WriteLine("b)");
            Console.WriteLine("c)");
            Console.WriteLine("d)");
            Console.WriteLine("e)");

            string answer = Console.ReadLine();

            if (answer == "a" || answer == "A" || answer == "1" || answer == "-1")
            {
                Console.WriteLine(1);
            }

            else if (answer == "b")
            {
                Console.WriteLine(2);
            }

            else if (answer == "c")
            {
                Console.WriteLine(3);
            }

            else if (answer == "d")
            {
                Console.WriteLine(4);
            }

            else
            {
                Console.WriteLine(5);
            }

            switch (answer)
            {
                // Kontrollime, kas string answer on võrdne
                case "a":
                case "A":
                case "1":
                case "-1":
                    Console.WriteLine(1);
                    break;
                case "b":
                case "B":
                    Console.WriteLine(2);
                    break;
                case "c":
                case "C":
                    Console.WriteLine(3);
                    break;
                case "d":
                case "D":
                    Console.WriteLine(4);
                    break;
                default:
                    Console.WriteLine(5);
                    break;
            }

            Console.ReadLine();
        }
    }
}
