﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Loend või massiiv. Mingi hulk näiteks numbreid, sõnu vms
namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            // Luuakse täisarvude massiiv numbers, millesse mahub 5 täisarvu
            // Loomise hetkel pean määrama, kui suure (mitu numbri mahub) massiivi teen
            int[] numbers = new int[5];

            // Massiivi indeksid algavad 0'st
            // Viimane indeks on alati 1 võrra väiksem kui massiivi maksimum elementide arv
            numbers[0] = 1;
            numbers[1] = 2;
            numbers[2] = -1;
            numbers[3] = -11;
            numbers[4] = 12;

            Console.WriteLine(numbers[0]);
            Console.WriteLine(numbers[1]);
            Console.WriteLine(numbers[2]);
            Console.WriteLine(numbers[3]);
            Console.WriteLine(numbers[4]);

            Console.WriteLine();

            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }

            Console.WriteLine();

            ////// Prindi kõik numbrid tagurpidi
            for (int i = numbers.Length - 1; i >= 0; i--)
            {
                Console.WriteLine(numbers[i]);
            }

            Console.WriteLine();

            //// Prindi kõik positiivsed numbrid
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] > 0)
                {
                    Console.WriteLine(numbers[i]);
                }
            }

            Console.WriteLine();

            // Loo teine massiiv 3le numbrile ja pane sinna esimese massiivi 3 esimest numbrit. Prindi teise massiivi numbrid ekraanile
            int[] secondNumbers = new int[3];

            //secondNumbers[0] = numbers[0];
            //secondNumbers[1] = numbers[1];
            //secondNumbers[2] = numbers[2];
            for (int i = 0; i < 3; i++)
            {
                secondNumbers[i] = numbers[i];
            }

            for (int i = 0; i < secondNumbers.Length; i++)
            {
                Console.WriteLine(secondNumbers[i]);
            }

            Console.WriteLine();

            // Loo kolmas massiiv 3le numbrile ja pane sinna esimese massiivi 3 numbrit tagant poolt alates. Prindi kolmanda massiivi numbrid ekraanile

            int[] thirdNumbers = new int[3];

            //thirdNumbers[0] = numbers[4];
            //thirdNumbers[1] = numbers[3];
            //thirdNumbers[2] = numbers[2];
            for (int i = 0; i < thirdNumbers.Length; i++)
            {
                thirdNumbers[i] = numbers[numbers.Length - 1 - i];
            }

            // või
            int a = 4;
            for (int i = 0; i < thirdNumbers.Length; i++)
            {
                thirdNumbers[i] = numbers[a];
                a--;
            }

            // või
            int j = 4;
            for (int i = 0; i < thirdNumbers.Length; i++, j--)
            {
                thirdNumbers[i] = numbers[a];
            }

            for (int i = 0; i < thirdNumbers.Length; i++)
            {
                Console.WriteLine(thirdNumbers[i]);
            }

            Console.ReadLine();
        }
    }
}
