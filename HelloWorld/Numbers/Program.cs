﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            short a = 1000;
            int b;
            
            // Implicit convertion
            // toimub iseenesest teisendus
            b = a;

            Console.WriteLine(a);
            Console.WriteLine(b);

            // Explicit convertion
            // pead ise teisendama (cast) 
            short c = (short)b;

            // Convert - teisendad eri tüüpide vahel (tekst ja number)
            // string to int

            // cast - teisendus (number ja number)
            // short to int
            // int to long

            Console.WriteLine(c);

            b = 35000;
            c = (short)b;

            Console.WriteLine(c);

            long d = 8123123123;

            b = (int)d;

            Console.WriteLine(b);


            byte g = 100;

            short h = 10000;

            g = (byte)h;

            Console.WriteLine(g);

            // int + short = int
            // int + long = long
            // int * long = long
            // short / byte = short
            // byte / short = short

            // Matemaatilistel tehetel kahe eri numbri tüübi vahel on tulemus alati suurem tüüp
            // välja arvatud tüübid, mis on väiksemad kui int, nende tehete tulemus on alati int

            byte t = 244;
            short e = 1000;

            Console.WriteLine(e / t);
            Console.WriteLine(t / e);

            

            Console.ReadLine();
        }
    }
}
