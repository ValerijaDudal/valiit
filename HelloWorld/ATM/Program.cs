﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ATM
{
    class Program
    {
        static string pin;

        static double balance;

        static void Main(string[] args)
        {
            // Faili kirjutamist ja failist lugemist teha alati võimalikult harva

            // See on klassi muutuja, kehtib terve klassi piires ehk kõikides klassi meetodites

            pin = LoadPin();
            balance = LoadBalance();

            if (!CheckPinCode())
            {
                return;
            }

            Console.WriteLine("Vali toiming:");
            Console.WriteLine("a) Sularaha sissemakse");
            Console.WriteLine("b) Sularaha väljamakse");
            Console.WriteLine("c) Kontojääk");
            Console.WriteLine("d) Katkesta");
            Console.WriteLine("e) Muuda PIN");
            Console.WriteLine("f) Tee ülekanne");

            string answer = Console.ReadLine();

            switch (answer)
            {
                case "a":
                    Console.WriteLine("Sisesta summa:");
                    int insertedSum = Convert.ToInt32(Console.ReadLine());
                    balance += insertedSum;
                    SaveBalance(balance);
                    Console.WriteLine("Summa {0} on kontole lisatud", insertedSum);

                    Console.WriteLine("Kas soovid kontrollida kontojääki?");
                    string wantPlusBalance = Console.ReadLine();
                    if(wantPlusBalance == "jah")
                    {
                        Console.WriteLine("Sinu kontojääk on {0}", balance);
                        Console.ReadLine();
                    }
                    else
                    {
                        break;
                    }
                    break;

                case "b":
                    Console.WriteLine("Vali summa:");
                    int removedSum = Convert.ToInt32(Console.ReadLine());
                    balance -= removedSum;
                    SaveBalance(balance);
                    Console.WriteLine("Summa {0} on väljastatud ja kontolt maha arvestatud", removedSum);

                    Console.WriteLine("Kas soovid kontrollida kontojääki?");
                    string wantLessBalance = Console.ReadLine();
                    if (wantLessBalance == "jah")
                    {
                        Console.WriteLine("Sinu kontojääk on {0}", balance);
                        Console.ReadLine();
                    }
                    else
                    {
                        break;
                    }

                    break;

                case "c":
                    Console.WriteLine("Sinu kontojääk on {0}", balance);
                    Console.ReadLine();
                   
                    break;

                case "d":
                    return;

                case "e":
                    if (!CheckPinCode())
                    {
                        return;
                    }

                    Console.WriteLine("Sisesta uus PIN");
                    string newPin = Console.ReadLine();                   
                    SavePin(newPin);
                    Console.WriteLine("PIN on salvestatud");
                    break;

                case "f":
                    Console.WriteLine("Sisesta saaja nimi:");
                    string name = Console.ReadLine();
                    Console.WriteLine("Sisesta saaja konto:");
                    string konto = Console.ReadLine();
                    Console.WriteLine("Sisesta summa");
                  

                    int amount = Convert.ToInt32(Console.ReadLine());
                    balance -= amount;
                    SaveBalance(balance);
                    Console.WriteLine("{0} eurot on kantud üle {1} kontole {2}", amount, name, konto);

                    Console.WriteLine("Kas soovid kontrollida kontojääki?");
                    string wantToKnowBalance = Console.ReadLine();
                    if (wantToKnowBalance == "jah")
                    {
                        Console.WriteLine("Sinu kontojääk on {0}", balance);
                        Console.ReadLine();
                    }
                    else
                    {
                        break;
                    }

                    break;                    
            }

            Console.ReadLine();

        }

        private static bool CheckPinCode()
        {
            for (int i = 1; i <= 3; i++)
            {
                Console.WriteLine("Sisesta PIN-kood:");
                string enteredPin = Console.ReadLine();
                if (!IsPinCorrect(enteredPin))
                {
                    Console.WriteLine("Vale PIN");
                    if (i < 3)
                    {
                        Console.WriteLine("Proovi uuesti!");
                    }
                    else
                    {
                        Console.WriteLine("Kaart on konfiskeeritud");
                        Console.ReadLine();
                        // Return lõpetab meetodi töö
                        return false;
                    }
                }
                else
                {
                    return true;
                }                  
            }
            return true;
        }

        static string LoadPin()
        {
            string pin = "";
            // Loeb failist
            // kustutame tühja rida, kui failis on see tekkinud
            pin = File.ReadAllText("Pin.txt").TrimEnd(new char[] { '\r', '\n' });
            return pin;

            //string[] lines = File.ReadAllLines("Pin.txt");
            //return lines[0];
            //// või
            //return File.ReadAllLines("Pin.txt")[0];
        }

        static void SavePin(string pin)
        {
            // Salvestatakse pin faili
            string pinString = Convert.ToString(pin);
            File.WriteAllText("Pin.txt", pinString);

            //"Peeter ja Jaan ; Juhan, Malle".Split(new string[] { " ja ", ", ", "; "}, StringSplitOptions.None);
        }

        static double LoadBalance()
        {
            double balance = 0;
            balance = Convert.ToDouble(File.ReadAllText("Balance.txt"));
            return balance;

            //// või
            //return Convert.ToDouble(File.ReadAllText("Balance.txt"));
        }

        static void SaveBalance(double balance)
        {
            // Salvestatakse balance faili
            string balanceString = Convert.ToString(balance);
            File.WriteAllText("Balance.txt", balanceString);             
        }

        static bool IsPinCorrect(string enteredPin)
        {
            if (enteredPin == pin)
            {
                return true;
            }
            return false;
        }
    }
}

