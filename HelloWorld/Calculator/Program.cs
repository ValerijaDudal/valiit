﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Küsi kasutajalt arvud, millega tahad tehteid teha, eralda arvud tühikuga (küsi ühe reaga)
            // Olenevalt sellest, kas kasutaja sisestas 2, 3 või enam arvu, paku kasutajale tehted, mida kasutaja saab nende arvudega teha

            // kui panna kaks arvu
            // Vali tehe
            // a) Liida
            // b) Lahuta
            // c) Korruta
            // d) Jaga

            Console.WriteLine("Sisesta arvud, eraldades neid tühikutega");

            string answer = Console.ReadLine();
            string[] numbersString = answer.Split(' ');

            int[] numbers = new int[numbersString.Length];

            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = Convert.ToInt32(numbersString[i]);
            }

            Console.WriteLine("Vali tehe:");

            if(numbers.Length == 1)
            {
                Console.WriteLine("Tehete jaoks on vaja vähemalt 2 numbrit");
            }

            else if (numbers.Length == 2)
            {
                Console.WriteLine("a) Lahuta");
                Console.WriteLine("b) Jaga");
                Console.WriteLine("c) Liida");
                Console.WriteLine("d) Korruta");

                string functionTwoNumbers = Console.ReadLine();

                if (functionTwoNumbers == "a")
                {
                    Console.WriteLine("Arvude {0} ja {1} vahe on {2}", numbers[0], numbers[1], Substract(numbers[0], numbers[1]));
                }

                else if (functionTwoNumbers == "b")
                {
                    Console.WriteLine("Arvude {0} ja {1} jagatis on {2}", numbers[0], numbers[1], Divide(numbers[0], numbers[1]));
                }

                else if (functionTwoNumbers == "c")
                {
                    Console.WriteLine("Arvude {0} ja {1} summa on {2}", numbers[0], numbers[1], Sum(numbers));
                }
                else
                {
                    Console.WriteLine("Arvude {0} ja {1} korrutis on {2}", numbers[0], numbers[1], Multiply(numbers[0], numbers[1]));
                }
            }

            else if (numbers.Length == 3)
            { 
                Console.WriteLine("a) Liida");
                Console.WriteLine("b) Korruta");

                string functionThreeNumbers = Console.ReadLine();

                if (functionThreeNumbers == "a")
                {
                    Console.WriteLine("Arvude {0}, {1} ja {2} summa on {3}", numbers[0], numbers[1], numbers[2], Sum(numbers));
                }
                else
                {
                    Console.WriteLine("Arvude {0}, {1} ja {2} korrutis on {3}", numbers[0], numbers[1], numbers[2], Multiply(numbers[0], numbers[1], numbers[2]));
                }
            }
            else
            {
                // Muudame massiivi listiks
                List<int> numbersList = numbers.ToList();
                // Kustutame viimase listi elemendi
                numbersList.RemoveAt(numbersList.Count - 1);

                Console.WriteLine("Arvude {0} ja {1} summa on {2}", string.Join(", ", numbersList), numbers[numbers.Length - 1], Sum(numbers));
            }



            //Console.WriteLine("Arvude 2, 6 ja 3 summa on {0}", Sum(2, 6, 3));

            //Console.WriteLine("Arvude 5 ja 3 vahe on {0}", Substract(5, 3));

            //Console.WriteLine("Arvude 5, 3 ja 2 korrutis on {0}", Multiply(5, 3, 2));

            //Console.WriteLine("Arvude 15 ja 3 jagatis on {0}", Divide(15, 3));

            //int[] numbers = new int[] { 1, 3, 5, 7 };

            //Console.WriteLine(Sum(numbers));

            Console.ReadLine();
        }

        static int Sum(int a, int b, int c = 0)
        {
            int sum = a + b + c;
            return sum;
            // või
            // return a + b + c
        }

        // Substract
        // Multiply
        // Divide

        // kui on tagastav meetod int (või mingi muu objekt), siis allpool peab olema return (mida me tagastame)
        static int Substract(int a, int b)
        {
            int substract = a - b;
            return substract;
            // return a - b
        }

        static int Multiply(int a, int b, int c = 1)
        {
            int multiply = a * b * c;
            return multiply;
            // return a * b * c
        }

        static double Divide(int a, double b)
        {
            return a / b;
        }

        // Loo meetod, mis liidab kõik massiivi numbrid kokku

        static int Sum(int[] numbers)
        {
            int count = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                count += numbers[i];
            }

            return count;
        }
    }
}
