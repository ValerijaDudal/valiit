﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsStack
{
    class Program
    {
        static void Main(string[] args)
        {
            // "Tõde ja õigus", "Pipi Pikksukk", "Kroonika"
            // Lisan hunnikusse raamatu "Kalevipoeg"
            // "Tõde ja õigus", "Pipi Pikksukk", "Kroonika", "Kalevipoeg"
            // Võtan hunnikust raamatu "Kalevipoeg"
            // "Tõde ja õigus", "Pipi Pikksukk", "Kroonika"
            // Võtan hunnikust raamatu "Kroonika"
            // "Tõde ja õigus", "Pipi Pikksukk"
            // Võtan hunnikust raamatu "Pipi Pikksukk"
            // "Tõde ja õigus"
            // Saab võtta ja lisada ainult viimast elementi

            Stack<string> stack = new Stack<string>();

            // Lisab elemente viimasele kohale
            stack.Push("Tõde ja õigus");
            stack.Push("Pipi Pikksukk");
            stack.Push("Kroonika");

            foreach (var book in stack)
            {
                Console.WriteLine(book);
            }

            Console.WriteLine();

            // Ütleb viimast elementi ja võtab seda ära
            string topBook = stack.Pop();
            Console.WriteLine($"Ülemine raamat oli {topBook}");

            Console.WriteLine();

            foreach (var book in stack)
            {
                Console.WriteLine(book);
            }

            Console.WriteLine();

            // Vaatab, mis element on viimane, aga ei võta ära
            Console.WriteLine($"Ülemine raamat on nüüd {stack.Peek()}");

            Console.WriteLine();

            foreach (var book in stack)
            {
                Console.WriteLine(book);
            }

            Console.ReadLine();
        }
    }
}
