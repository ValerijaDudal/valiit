﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintHello();
            PrintHello();
            PrintHello();

            PrintText("Tere kuidas elad?");
            PrintText("Elan hästi!");

            PrintHello(5);

            PrintText("Õues on ilus ilm", 5);

            PrintText("Happy Birthday ", 5, false);

            PrintText("");

            PrintText("Happy Birthday ", 5, true);

            

            Console.ReadLine();
        }

        static void PrintHello()
        {
            Console.WriteLine("Hello");
        }

        //static void PrintText(string text)
        //{
        //    Console.WriteLine(text);
        //}

        static void PrintHello(int howManyTimes)
        {
            for (int i = 0; i < howManyTimes; i++)
            {
                PrintHello();
                // või
                //Console.WriteLine("Hello!");
            }
        }

        // Meetod PrintText, kus lisaks tekstile on parameeter, mis küsib mitu korda printida

        //static void PrintText(string text, int howManyTimes)
        //{
        //    for (int i = 0; i < howManyTimes; i++)
        //    {
        //        PrintText(text);
        //        // või
        //        //Console.WriteLine(text);
        //    }
        //}

        // Veel üks meetod PrintText, mis lisaks tekstile ja korduste arvule omab kolmandat parameetrit, mis määrab ära, kas need korduvad tekstid prinditakse ühele või mitmele reale

        //static void PrintText(string text, int howManyTimes, bool newLine)
        // vaikeväärtused ehk kui ise ei määra siis on need väärtused, saab panna lõpust alates igale elemendile
        static void PrintText(string text = "", int howManyTimes = 1, bool newLine = false)
        {

            for (int i = 0; i < howManyTimes; i++)
            {
                if (!newLine)
                {
                    Console.Write(text);
                }
                else
                {
                    Console.WriteLine(text);
                }
            }
        }
    }
}
