﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();
            car.Model = "525";
            car.Make = "BMW";
            car.Year = 1777;

            car.StartEngine();

            //Console.WriteLine("Kui kiiresti soovid sõita?");
            //int speed = Convert.ToInt32(Console.ReadLine());
            //car.SpeedUp(speed);
            


            //Console.WriteLine("Kui aglaselt soovid sõita?");
            //speed = Convert.ToInt32(Console.ReadLine());
            //car.SpeedDown(speed);


            //Console.WriteLine("Kas soovid teha auto diagnostikat?");
            //string answer = Console.ReadLine();
            //if(answer == "jah")
            //{
            //    car.DiagnosticsRequest();
            //    if (car.OilLevel < 50)
            //    {
            //        Console.WriteLine("Kas soovid lisada õli?");
            //        string answerOil = Console.ReadLine();
            //        if (answerOil == "jah")
            //        {
            //            car.AddOil();
            //        }
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("Sõidame edasi!");
            //}




            //Console.WriteLine("Kas soovid autod välja lülitada?");
            //answer = Console.ReadLine();
            //if (answer == "jah")
            //{
            //    car.EngineStop();
            //}
            //else
            //{
            //    Console.WriteLine("Sõidame edasi!");
            //}


            Person driver = new Person();
            driver.FirstName = "Kalle";
            driver.LastName = "Kaalikas";
            driver.Age = 25;
            driver.Gender = Gender.Male;

            Person owner = new Person()
            {
                FirstName = "Malle",
                LastName = "Maasikas",
                Age = 30,
                Gender = Gender.Female
            };

            car.Driver = driver;
            car.Owner = owner;

            // Prindi välja auto omaniku perekonnanimi

            Console.WriteLine("Auto {0} omaniku perekonnanimi on {1}", car.Make, car.Owner.LastName);

            // Lisa autosse kolm reisijat ja prindi välja nende vanused
            // Lisage uus person, lisa temale ema ja isa ning lisa see sama person auto autojuhiks
            // autojuhi emale ja isale lisa samuti isa
            // ning küsi auto objekti käest mis on selle autojuhi mõlema vanaisade eesnimed

            var passengers = new List<Person>();
            passengers.Add(new Person()
            {
                FirstName = "Jüri",
                LastName = "Juurikas",
                Age = 32
            });
            passengers.Add(new Person()
            {
                FirstName = "Liisa",
                LastName = "Lusikas",
                Age = 28
            });
            passengers.Add(new Person()
            {
                FirstName = "Ann",
                LastName = "Pirnipuu",
                Age = 25
            });

            car.Passengers = passengers;

            foreach (var passenger in car.Passengers)
            {
                Console.WriteLine(passenger.Age);
            }

            Person newDriver = new Person();
            newDriver.FirstName = "Karl";
            newDriver.LastName = "Vares";
            newDriver.Age = 30;
            newDriver.Gender = Gender.Male;

            Person karlMother = new Person();
            karlMother.FirstName = "Malle";
            karlMother.LastName = "Vares";
            karlMother.Age = 55;
            karlMother.Gender = Gender.Female;

            Person karlFather = new Person();
            karlFather.FirstName = "Andrus";
            karlFather.LastName = "Vares";
            karlFather.Age = 60;
            karlFather.Gender = Gender.Male;

            Person mothersFather = new Person();
            mothersFather.FirstName = "Indrek";
            mothersFather.LastName = "Vares";
            mothersFather.Age = 75;
            mothersFather.Gender = Gender.Male;

            Person fathersFather = new Person();
            fathersFather.FirstName = "Sven";
            fathersFather.LastName = "Vares";
            fathersFather.Age = 80;
            fathersFather.Gender = Gender.Male;

            car.Driver = newDriver;
            car.Driver.Mother = karlMother;
            car.Driver.Father = karlFather;
            car.Driver.Mother.Father = mothersFather;
            car.Driver.Father.Father = fathersFather;

            // Autojuhi vanaisa nime muutmine
            car.Driver.Father.Father.FirstName = "Peeter";

            Console.WriteLine(car.Driver.Mother.Father.FirstName);
            Console.WriteLine(car.Driver.Father.Father.FirstName);

            car.Driver.Father.Father.Father = new Person
            {
                FirstName = "John",
                Father = new Person
                {
                    FirstName = "Jake"
                }

            };

            Console.WriteLine(car.Driver.Father.Father.Father.FirstName);
            Console.WriteLine(car.Driver.Father.Father.Father.Father.FirstName);

            Console.WriteLine();

            Person.PrintFatherName(car.Driver);

            Console.ReadLine();
        }
    }
}
