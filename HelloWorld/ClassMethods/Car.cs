﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassMethods
{
    class Car
    {
        // private tähendab, et muutuja või meetod on ligipääsetav ainult selle sama klassi sees
        private int year;
        public string Model { get; set; }
        public string Make { get; set; }
        public int Year
        {
            get
            {
                return year;
            }
            set
            {
                if(value < 1880)
                {
                    year = 1880;
                }
                else if(value > DateTime.Now.Year)
                {
                    year = DateTime.Now.Year;
                }
                else
                {
                    year = value;
                }
                
            }
        }

        // private set tähendab, et seda set meetodit saab välja kutsuda ainult klassi enda seest
        public bool isEngineRunning { get; private set; }

        public void StartEngine()
        {
            if(isEngineRunning)
            {
                Console.WriteLine("Mootor juba töötab");
            }
            else
            {
                Console.WriteLine("Mootor käivitus");
                isEngineRunning = true;
            }
        }

        // lisage property, kus hoitakse auto hetke kiirust ja kust saab küsida
        // lisage meetod kiirendamiseks, kus saab määrata kui suure kiiruseni kiirendan
        // lisage meetod aeglustamiseks
        // lülita mootor välja
        // Tee diagnostika (kontrollib õli taset, tosooli taset protsentides. Kui vähem kui 50%, siis lisa juurde)

    
        
        public int Speed { get; private set; }

        public Person Owner { get; set; }
        public Person Driver { get; set; }
        public List<Person> Passengers { get; set; }

        

        //public void SpeedUp(int wantSpeed)
        //{
        //    if (!isEngineRunning)
        //    {
        //        Console.WriteLine("Auto mootor ei tööta, ei saa hetkel kiirendada");
        //    }
        //    else if (Speed > wantSpeed)
        //    {
        //        Console.WriteLine("Ei saa kiirendada aeglasemale kiirusele");
        //    }
        //    else if (wantSpeed < 0)
        //    {
        //        Console.WriteLine("Auto kiirus ei saa olla negatiivne");
        //    }
        //    else if (wantSpeed > 400)
        //    {
        //        Console.WriteLine("Auto nii kiiresti ei sõida");
        //    }
        //    else
        //    {
        //        Speed = wantSpeed;
        //        Console.WriteLine("Auto kiirus on nüüd {0}", Speed);
        //    }
            
        //}

        //public void SpeedDown(int wantSpeed)
        //{
        //    if (!isEngineRunning)
        //    {
        //        Console.WriteLine("Auto mootor ei tööta, ei saa hetkel kiirendada");
        //    }
        //    else if(Speed < wantSpeed)
        //    {
        //        Console.WriteLine("Ei saa aeglustada kiiremale kiirusele");
        //    }
        //    else if (wantSpeed < 0)
        //    {
        //        Console.WriteLine("Auto kiirus ei saa olla negatiivne");
        //    }
        //    else if (wantSpeed > 400)
        //    {
        //        Console.WriteLine("Auto nii kiiresti ei sõida");
        //    }
        //    else
        //    {
        //        Speed = wantSpeed;
        //        Console.WriteLine("Auto aeglustus kuni kiiruseni {0}", Speed);
        //    }

        //}


        //int oilLevel;
        //int tosolLevel;


        //public int OilLevel { get; private set; }

        //public void DiagnosticsRequest()
        //{
        //    Random random = new Random();
        //    oilLevel = random.Next(0, 100);

        //    Random randomSecond = new Random();
        //    tosolLevel = randomSecond.Next(0, 100);

        //    Console.WriteLine("Õli tase on {0}, tosooli tase on {1}", oilLevel, tosolLevel);
        //}

   
        //public void AddOil()
        //{         
        //    oilLevel = 100;
        //    Console.WriteLine("Õli tase on nüüd {0}", oilLevel);
        //}







        //public void EngineStop()
        //{
        //    Console.WriteLine("Mootor on välja lülitatud");
        //}
    }
}
