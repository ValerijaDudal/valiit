﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassMethods
{
    enum Gender { Male, Female, Undisclosed }

    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public int Age { get; set; }
        public Person Mother { get; set; }
        public Person Father { get; set; }

        // Rekursiivne meetod ehk meetod mis kutsub ise ennast uuesti välja
        // Staatiline meetod on selline meetod, mille välja kutsumiseks ei ole objekti välja tuua (meetod ei ole mingi objektiga seotud)
        static public void PrintFatherName(Person person)
        {
            if(person.Father != null)
            {
                Console.WriteLine(person.Father.FirstName);
                PrintFatherName(person.Father);
            }
        }
     
    }
}
