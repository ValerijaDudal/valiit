﻿// Annab ligipääsu kõigile klassidele, mis asuvad System nimeruumis
using System;

namespace HelloWorld // kõik klasside nimed, mis asuvad HelloWorld nimeruumi sees, kehtivad selle nimeruumi piires
{
    class Program // klass nimega programm
    {
        // staatiline meetod
        // void - meetod ei tagasta midagi
        // main - eriline meetod, millest programm alustab tööd
        // string[] args - meetodi parameetrid (argumendid)
        static void Main(string[] args)
        {
            // WriteLine() - Kutsutakse välja Console klassi meetod nimega WriteLine()
            // "Hello World!" - Meetodi WriteLine() parameeter ehk tekst, mida välja printida
            Console.WriteLine("Hello World!");
            // ReadLine() - Ootab kasutajalt vastust ehk teksti, mis lõppeb enteriga
            Console.ReadLine();
            // Kutsub välja nimeruumist Printer klassi Writer meetodi Write()
            Printer.Writer.Write();
            // Kutsub välja nimeruumist File klassi Writer meetodi Write()
            File.Writer.Write();
            // Kutsub välja nimeruumist Database klassi Writer meetodi Write()
            Database.Writer.Write();
        }
    }
}


namespace Printer
{
    public class Writer
    {
        public static void Write()
        {
        }
    }
}

namespace Database
{
    public class Writer
    {
        public static void Write()
        {
        }
    }
}

namespace File
{
    public class Writer
    {
        public static void Write()
        {
        }
    }
}

