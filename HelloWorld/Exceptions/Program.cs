﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Exception on programmi töös esinev erijuht, mille esinemisega ma peaks arvestama ja tegelema
namespace Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            //// Try plokk eraldab koodi osa, kus me arvame, et võib juhtuda exception
            //try
            //{
            //    string word = null;
            //    word.Split(' ');
            //}

            //// Püüab kinni erandi tüübist NullReferenceException ehk selline tüüp erandist, mis juhtub kui tahame
            //// kas mingit meetodit või parameetrit küsida milleltki, mis on parasjagu null
            //catch (NullReferenceException ex)
            //{
            //    Console.WriteLine("Viga word on null: " + ex.Message);
            //}

            int a = 5;
            int b = 8;

            // Püüa exception kinni ja kirjuta kasutajale: Nulliga ei saa jagada
            try
            {
                Console.WriteLine("Sisesta arv, millega tahad numbrit 5 jagada");
                b = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Arvude 5 ja {0} jagatis on {1}", b, a / b);
                int[] numbers = new int[2];
                numbers[2] = 3;
            }
            //catch (DivideByZeroException ex)
            //{

            //    Console.WriteLine("Nulliga ei saa jagada");
            //}
            //catch (FormatException ex)
            //{
            //    Console.WriteLine("Sisestatud väärtus ei olnud number");
            //}
            //catch (IndexOutOfRangeException ex)
            //{
            //    Console.WriteLine("Süsteemi erand");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("Juhtus mingi muu erand {0}", ex.Message);
            //}

            catch (Exception ex)
            {
                if (ex is FormatException || ex is DivideByZeroException)
                {
                    Console.WriteLine("Vales formaadis või nulliga jagamine");
                }
                else
                {
                    Console.WriteLine("Juhtus mingi muu erand {0}", ex.Message);
                }
            }


            Console.ReadLine();
        }   
    }
}
