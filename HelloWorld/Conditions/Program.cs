﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conditions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Sisesta palun 1 number");

            string answer = Console.ReadLine();

            // asendab tekstis kõik komad punktidega
            answer = answer.Replace(",", ".");

            Console.WriteLine(CultureInfo.CurrentCulture.Name);

            double a = Convert.ToDouble(answer, CultureInfo.InvariantCulture);

            // Seadista programmi keeleks USA inglise keel (süsteemi keel jääb samaks)
            CultureInfo.CurrentCulture = new CultureInfo("en-US");

            // Kontrolli, kas programmi (süsteemi) keel on Eesti eesti keel
            if (CultureInfo.CurrentCulture.Name == "et-EE")
            {
                // kasuta koma reaalarvus
                a = Convert.ToDouble(answer);
            }
            else
            {
                // kasuta punkti reaalarvus
                // lisaparameeter konverteerimise keelena
                a = Convert.ToDouble(answer, new CultureInfo("en-US"));
            }

            Console.WriteLine(a);

            if(a == 3)
            {
                Console.WriteLine("Arv on võrdne kolmega");
            }

            if (a != 4)
            {
                Console.WriteLine("Arv ei ole võrdne neljaga");
            }

            if (a > 2)
            {
                Console.WriteLine("Arv on suurem kahest");
            }

            // Kui arv on suurem võrdne 5ga
            if (a >= 5)
            {
                Console.WriteLine("Arv on suurem võrdne 5ga");
            }

            
            // VÕI ||
            // JA &&

            // Kui arv on 2 ja 8 vahel
            if (a > 2 && a < 8)
            {
                Console.WriteLine("Arv on 2 ja 8 vahel");
            }

            // Kui arv on väiksem kui 2 või suurem kui 8
            if (a < 2 || a > 8)
            {
                Console.WriteLine("Arv on väiksem kui 2 või suurem kui 8");
            }

            // Kui arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10
            if ((a > 2 && a < 8) || (a > 5 && a < 8) || a > 10)
            {
                Console.WriteLine("Arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10");
            }

            Console.ReadLine();
            
        }
    }
}
