﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoWhile_loop
{
    class Program
    {
        static void Main(string[] args)
        {
            bool exitProgram = false;

            while (!exitProgram)
            {
                Console.WriteLine("Tere");
                Console.WriteLine("Kas soovid jätkata? j/e");
                if (Console.ReadLine() != "j")
                {
                    exitProgram = true;
                }
            }

            // do while tsükkel erineb tavalisest selle poolest, et esimene tsükkli kordus tehakse alati, olenemata kas kontrollitav tingimus on tõene või vale
            // Kasutatakse siis, kui mingi tegevus on tehtud ning tahan otsustada, kas teen seda veel
            do
            {
                Console.WriteLine("Tere");
                Console.WriteLine("Kas soovid jätkata? j/e");

            } while (Console.ReadLine() == "j");

        }
    }
}
