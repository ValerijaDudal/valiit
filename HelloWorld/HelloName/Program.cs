﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloName
{
    class Program
    {
        static void Main(string[] args)
        {
            //// Defineerime muutuja tüübist string (teksti sisaldav muutuja tüüp) nimega ´name`
            //string name;
            //// Anname muutujale ´name` väärtuse "Valerija"
            //name = "Valerija";

            //// Escape \" laseb kasutada stringi sees " sümbolit
            //// Escape \\ laseb kasutada stringi sees \ sümbolit
            //string greeting = "Tere \"" + name + "\"!";

            //Console.WriteLine(greeting);

            //// Tühi rida
            //Console.WriteLine();

            //Console.WriteLine("Mis on sinu nimi?");

            //name = Console.ReadLine();

            //greeting = "Tere " + name + "!";

            //Console.WriteLine(greeting);



            Console.WriteLine("Mis on sinu eesnimi?");

            string firstName = Console.ReadLine();

            string greeting = "Tere " + firstName + "!";

            Console.WriteLine(greeting);

            Console.WriteLine();

            Console.WriteLine("Mis on sinu perekonnanimi?");

            string lastName = Console.ReadLine();

            greeting = "Tere " + firstName + " " +lastName + "!";

            Console.WriteLine(greeting);

            Console.ReadLine();
        }
    }
}
