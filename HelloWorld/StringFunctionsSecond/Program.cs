﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringFunctionsSecond
{
    class Program
    {
        static void Main(string[] args)
        {
            //string sentencee = "Elas metsas Mutionu";

            //// Kasutades meetodeid Index.Of ja Substring prindi ekraanile lause esimene sõna


            //// Leidsime esimese tühiku
            //int spaceIndexx = sentencee.IndexOf(" ");

            //// Kirjutasime kõik elemendid 0'st kuni tühikuni
            //string firstWord = sentencee.Substring(0, spaceIndexx);

            //Console.WriteLine(firstWord);



            //// Kasutades meetodeid Index.Of ja Substring prindi ekraanile lause teine sõna

            //// Otsime teise tühiku alates esimese tühiku asukohast
            //int spaceIndexSecondd = sentencee.IndexOf(" ", spaceIndexx + 1);

            //Console.WriteLine(spaceIndexSecondd);

            //// Kirjutame kõik elemendid alates esimesest elemendist peale esimest tühikut kuni teise tühikuni. Peale koma on elementide arv, mitte indeks, seepärast lahutame teise tühiku indeksist
            //// teise tühiku indeksit
            //string secondWordd = sentencee.Substring(spaceIndexx + 1, spaceIndexSecondd - (spaceIndexx + 1));

            //Console.WriteLine(secondWordd);






            //Console.WriteLine("Sisesta lause");

            //string sentence = Console.ReadLine().Trim();

            //// Otsime esimest tühikut, mida ei ole, vastus -1

            //if (sentence.Length == 0)
            //{
            //    c
            //}

            //else
            //{

            //    int spaceIndex = sentence.IndexOf(" ");
            //    if (spaceIndex == -1)
            //    {
            //        Console.WriteLine("Esimene sõna on {0}", sentence);
            //        Console.WriteLine("Teine sõna on puudu");
            //    }
            //    else
            //    {
            //        Console.WriteLine("Esimene sõna on {0}", sentence.Substring(0, spaceIndex));
            //        int secondSpaceIndex = sentence.IndexOf(" ", spaceIndex + 1);
            //        if(secondSpaceIndex == -1)
            //        {
            //            // Substring 1 parameetriga on parameetrist kuni lõpuni
            //            Console.WriteLine("Teine sõna on {0}", sentence.Substring(spaceIndex + 1));
            //        }
            //        else
            //        {
            //            Console.WriteLine("Teine sõna on {0}", sentence.Substring(spaceIndex + 1, secondSpaceIndex - spaceIndex - 1));
            //        }
            //    }
            //}



            // Kasuta string spliti

            // Kui string.Split ei leia seda sümbolit, millega ma tahan tükeldada stringi, siis ta tagastab massiivi ühe elemendiga
            // ja see üks element ongi esialgne terve lause
            //string[] words = sentence.Split(' ');


            //// trim() võtab tühikud ära




            //if (words.Length == 0)
            //{
            //    Console.WriteLine("Lauses pole sõnu");

            //}

            //else
            //{
            //    Console.WriteLine("Esimene sõna on {0}", words[0]);

            //    if(words.Length > 1)
            //    {
            //        Console.WriteLine("Teine sõna on {0}", words[1]);
            //    }

            //    else
            //    {
            //        Console.WriteLine("Teine sõna on puudu");
            //    }
            //}












            //// Prindi teine sõna
            //string sentenceThird = "Elas metsas";

            //int spaceIndexAgain = sentenceThird.IndexOf(" ");

            //int totalIndex = sentenceThird.Length;

            //string wordSecond = sentenceThird.Substring(spaceIndexAgain + 1, (totalIndex) - (spaceIndexAgain + 1));

            //Console.WriteLine("'" + wordSecond + "'");



            //Console.ReadLine();













            //// Leia sümbolite arv
            //Console.WriteLine("Lauses on {0} sümbolit", sentence.Length);

            //// Leia esimese tühiku (või muu sümboli) asukoht (index)
            //int spaceIndex = sentence.IndexOf(" ");

            //Console.WriteLine("Esimese tühiku indeks on {0}", spaceIndex);

            //// Leia esimesest 3 sümbolist koosnev tekstiosa
            //string subString = sentence.Substring(0, 3);

            //Console.WriteLine("Esimesest 3 sümbolist koosnev tekstiosa on {0}", subString);

            //string word = "raudtee";

            //// char asendus
            //Console.WriteLine(word.Replace('r', 'R'));

            //// stringi asendus
            //Console.WriteLine(word.Replace("raud", "asfalt"));

            //// asendan komad ja tühikud, et kõik oleks kokku kirjutatud
            //Console.WriteLine("Mina, Pets, Margus, Priit".Replace(",", "").Replace(" ", ""));

            //// Asendame täpitähed tavaliste tähtedega
            //string someString = "Põder jooksis öösel üle ääre";
            //Console.WriteLine(someString.Replace('õ', 'o')
            //    .Replace('ö', 'o')
            //    .Replace('ü', 'u')
            //    .Replace('ä', 'a'));

            //Console.WriteLine("Sisesta oma kaal");
            //// 79.45
            //// 79,45
            //// Asendame komad punktidega
            //double weight = Convert.ToDouble(Console.ReadLine().Replace(',', '.'), new CultureInfo("en-US"));

            //// :0.00 kohahoidjale juurde pannes saame öelda, et ümarda 2 kohta peale
            //// :0.000 kohahoidjale juurde pannes saame öelda, et ümarda 3 kohta peale
            //// :0.000 kohahoidjale juurde pannes saame öelda, et ümarda 4 kohta peale
            //// :0 kohahoidjale juurde pannes saame öelda, et ümarda täisarvuni
            //Console.WriteLine("Kaal on {0:0.00}", weight);

            
            // DateTime.Now.Day-3 => kuupäev 3 päeva tagasi

            //for (int i = 0; i < 1000; i++)
            //{
            //    Console.WriteLine("Arve OA{0}{1:00}{2:00}{3:000000000}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, i);
            //}
        
            //Console.WriteLine("Sisesta oma tel nr");

            //// +37258455771
            //// 58455771 - õige
            //// 584 55 771
            //// 58 45 57 71

            //string phoneNumber = Console.ReadLine().Replace(" ", "").Replace("+372", "");
            //Console.WriteLine("Sinu tel nr on {0}", phoneNumber);


            


            Console.ReadLine();
        }
    }
}
