﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VariableScope
{
    class Program
    {
        // Kõik muutujad mis ma defineerin kehtivad/elavad {} piires või sees

        static void Main(string[] args)
        {
            int a = 4;

            Increment(a);
            Console.WriteLine("Value of a is {0}", a);

            string sentence = "Mis sa teed";
            AddQuestionMark(sentence);
            Console.WriteLine("Value of sentence is {0}", sentence);

            Console.WriteLine();

            int[] integers = new int[] { 2, 4, 23, -11, 12 };

            PrintNumbers(integers);
            Console.WriteLine();

            Increment(integers);
            Console.WriteLine();

            PrintNumbers(integers);

            // muudame b, aga c jääb samaks, kuna tegemist on muutuja lihttüübiga Value Type
            int b = 3;
            int c = b;
            b = 7;

            Console.WriteLine();

            Console.WriteLine(c);

            // muudame secondIntegers, muutub ka integers, kuna tegemist on Reference Type muutujaga 
            int[] secondIntegers = integers;
            secondIntegers[0] = -7;

            Console.WriteLine();

            Console.WriteLine(integers[0]);

            // muudame firstWord, aga secondWord jääb samaks, kuna tegemist on muutuja lihttüübiga Value Type
            string firstWord = "raud";
            string secondWord = firstWord;
            firstWord = "plastmass";

            Console.WriteLine();

            Console.WriteLine(secondWord);

            // muudame numbers, muutub ka secondnumbers, kuna tegemist on Reference Type muutujaga
            List<int> numbers = new List<int>() { 2, -4, 0 };
            IList<int> secondNumbers = numbers;
            numbers.RemoveAt(2);

            Console.WriteLine();

            for (int i = 0; i < secondNumbers.Count; i++)
            {
                Console.WriteLine(secondNumbers[i]);
            }

            Console.ReadLine();
        }

        // Kui meetodile anda parameetriks kaasa lihttüüpi 'Value Type' muutuja väärtus, siis tegelikult tehakse uus muutuja ja pannakse sinna sama väärtus
        // Kui meetodile anda parameetriks kaasa Reference Type muutuja, siis antakse kaasa tegelikult sama muutuja (uus muutuja mis viitab samale mäluaadressile)

        // Value Type - väärtusmuutuja: int, char, string, double, float, bool, short, long, decimal
        // Reference Type - viitmuutuja: massiivid, listid, dictionary, kõik klassid

        static void Increment(int number)
        {
            number++;
            Console.WriteLine("New number is {0}", number);
        }

        static void AddQuestionMark(string sentence)
        {
            sentence = sentence + "?";
            Console.WriteLine("New sentence is {0}", sentence);
        }

        static void PrintNumbers(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }
        }

        static void Increment(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i]++;
            }
        }
    }
}
