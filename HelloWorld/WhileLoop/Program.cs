﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            // Lõpmatu while tsükkel
            //while(true)
            //{
            //    Console.WriteLine("Hello");
            //}
            // Juhusliku arvu generaatori objekti loomine

            bool isGameOver = false;

            while (!isGameOver)
            {
                Random random = new Random();

                // arvud 1, 2, 3, 4, 5
                int correctNumber = random.Next(1, 6);
                int number = -999999;
                bool isFirstTry = true;

                // Kui kasutaja pakub midagi muud, kui 1 ja 5 vahel numbri
                // Kirjuta kasutajale. Proovi uuesti, sest pakutud number ei olnud 1 ja 5 vahel

                // Täienda programmi nii, 
                // et õige numbri arvamisel küsib kasutajalt kas ta tahab 
                // uuesti mängu mängida. Kui tahab, mõtleb uue numbri ja laseb uuesti arvata.


                while (number != correctNumber)
                {
                    if (!isFirstTry && (number < 1 || number > 5))
                    {
                        Console.WriteLine("Proovi uuesti, sest pakutud number ei olnud 1 ja 5 vahel");
                    }
                    else
                    {
                        Console.WriteLine("Arva ära number 1 ja 5 vahel");
                        isFirstTry = false;
                    }

                    number = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine("Õige number oli {0}", number);

                Console.WriteLine("Kas soovid uuesti arvata?");
                Console.WriteLine("Vasta j/e");

                string answer = Console.ReadLine();
                if (answer.ToLower() != "j")
                {
                    isGameOver = true;
                }
            }
        }
    }
}
