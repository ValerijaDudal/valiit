﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContinueAndBreak
{
    class Program
    {
        static void Main(string[] args)
        {
            //while (true)
            //{
            //    Console.WriteLine("Head\nAega");

            //    Console.WriteLine("Kas soovid jätkata? Vasta j/e");

            //    if (Console.ReadLine().ToLower() != "j")
            //    {
            //        break;
            //    }
            //}

            // Tee for tsükkel mis kirjutab numbrid 1 kuni 10 ja 5 jätab vahele kasutades continue

            for (int i = 1; i < 11; i++)
            {
                if(i == 5 || i == 7)
                {
                    continue;
                }

                Console.WriteLine(i);
           
            }

            Console.ReadLine();
        }

        
    }
}
