﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleArguments
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1. Sõna
            // 2. Mitu korda tahad sõna printida
            // 3. Kui kasutaja ei ole kaks parameetrit sisestanud, siis prinditakse kasutajale 

            if (args.Length != 2)

            {

                Console.WriteLine("Kasutus: {0} [sõna] [mitu korda]", Assembly.GetExecutingAssembly().GetName().Name);

            }

            else

            {
                int count;
                bool canParse = int.TryParse(args[1], out count);

                if(canParse)
                {
                    for (int i = 0; i < count; i++)
                    {
                        Console.WriteLine(args[0]);
                    }
                }

                else
                {
                    Console.WriteLine("Kasutus: {0} [sõna] [mitu korda]", Assembly.GetExecutingAssembly().GetName().Name);
                }
                
          
            }
        }
    }
}
