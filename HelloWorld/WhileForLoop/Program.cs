﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileForLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            // Prindi ekraanile arvud 1 kuni 5 while tsüklina
            // Küsi kasutajalt, mis päev täna on kuni ta ära arvab


            for (int i = 1; i < 6; i++)
            {
                Console.WriteLine(i);
            }


            int i = 1;
            while (i < 6)

            {
                Console.WriteLine(i);
                i++;
            }


            string weekDay = "neljapäev";
            string answer = "";

            while (weekDay.ToLower() != answer.ToLower())
            {
                Console.WriteLine("Mis päev täna on?");
                answer = Console.ReadLine();
            }


            for (; "neljapäev" != answer.ToLower(); )
            {
                Console.WriteLine("Mis päev täna on?");
                answer = Console.ReadLine();
            }



            Console.ReadLine();
        }
    }
}
