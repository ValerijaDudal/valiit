﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    interface ICanFly
    {
        // Määran ära, et igas klassis, mis seda interface implementeerib peab olema meetod Fly(), mis ei tagasta midagi ehk void Fly()
        void Fly();
    }
}
