﻿using Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    class Bird : ICanFly, IBirdOrCar, ICanFlyAndLayEggs
    {
        public void Fly()
        {
            JumpUp();
            Console.WriteLine("Bird is flying");
        }

        //public void LayEggs()
        //{
        //    Console.WriteLine("Lay eggs");
        //}

        private void JumpUp()
        {
            Console.WriteLine("Jumped up");
        }
    }
}
