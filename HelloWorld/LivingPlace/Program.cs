﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inheritance;

namespace LivingPlace
{
    class Program
    {
        static void Main(string[] args)
        {
            ILivingPlace livingPlace = new Forest();
            livingPlace.AddAnimal(new Pig());
            livingPlace.AddAnimal(new Goat());
            livingPlace.AddAnimal(new Pig());
            livingPlace.AddAnimal(new Goat());
            livingPlace.AddAnimal(new Pig());
            livingPlace.AddAnimal(new Goat());      
            livingPlace.AddAnimal(new Cow());
            livingPlace.AddAnimal(new Goat());   
            livingPlace.AddAnimal(new Cow());
            livingPlace.AddAnimal(new Cow());

            livingPlace.PrintAnimals();

            Console.WriteLine();

            livingPlace.RemoveAnimal("Pig");
            livingPlace.RemoveAnimal("Goat");
            livingPlace.RemoveAnimal("Chicken");

            Console.WriteLine();

            livingPlace.PrintAnimals();

            Console.WriteLine();

            livingPlace.GetAnimalCount("Pig");
       

            Console.ReadLine();
        }
    }
}
