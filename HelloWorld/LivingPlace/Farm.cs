﻿using Inheritance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivingPlace
{
    public class Farm : ILivingPlace
    {
        private List<FarmAnimal> animals = new List<FarmAnimal>();
        private Dictionary<string, int> animalCounts = new Dictionary<string, int>();

        public int MaxAnimalCount { get; private set; }

        public Farm(int count = 25)
        {
            MaxAnimalCount = count;
        }

        // 1. Lisada loomi
        public void AddAnimal(Animal animal)
        {
            // is kontrollib kas võrreldav objekt animal pärineb või on tüübist FarmAnimal
            if(!(animal is FarmAnimal))
            {
                Console.WriteLine("Farmis saavad olla ainult farmi loomad");
                return;
            }
            if (animals.Count < MaxAnimalCount)
            {
                if (!animalCounts.ContainsKey(animal.GetType().Name) && animalCounts.Count < 4)
                {
                    animals.Add((FarmAnimal)animal);
                    animalCounts.Add(animal.GetType().Name, 1);
                    Console.WriteLine("{0} on lisatud farmi", animal.GetType().Name);
                }

                else if (animalCounts.ContainsKey(animal.GetType().Name) && animalCounts[animal.GetType().Name] < 5)
                {
                    animals.Add((FarmAnimal)animal);
                    animalCounts[animal.GetType().Name]++;
                    Console.WriteLine("{0} on lisatud farmi", animal.GetType().Name);
                }

                else
                {
                    Console.WriteLine("Loom ei mahu farmi");
                }
            
            }
        }

        // 2. Küsida, mis loomad on
        // prindib loomatüübi koos arvuga
        // Cow 4
        // Pig 2
        public void PrintAnimals()
        {
            foreach (var animalCount in animalCounts)
            {
                Console.WriteLine("{0} => {1}", animalCount.Key, animalCount.Value);
            }
        }
        
        // 3. Küsida konkreetse looma arvu
        public int GetAnimalCount(string animalType)
        {
            if (animalCounts.ContainsKey(animalType))
            {
                Console.WriteLine(animalCounts[animalType]);
                return animalCounts[animalType];
            }
            
                Console.WriteLine("Looma ei leitud");
                return 0;
                
        }
        
        // 4. Eemaldada loomi
        // Kui on viimane loom ehk dictionarys on kogus 1, siis eemalda dictionaryst
        // muul juhul vähenda kogust 1 võrra
        public void RemoveAnimal(string animalType)
        {
            if (!animalCounts.ContainsKey(animalType))
            {
                Console.WriteLine("Looma {0} ei leitud", animalType);
                return;
            }
            else
            {
                for (int i = 0; i < animals.Count; i++)
                {
                    if (animals[i].GetType().Name == animalType)
                    {
                        if (animalCounts[animalType] == 1)
                        {
                            animalCounts.Remove(animalType);
                            Console.WriteLine("{0} on eemaldatud farmist", animalType);
                        }
                        else
                        {
                            animalCounts[animalType]--;
                            Console.WriteLine("{0} on eemaldatud farmist", animalType);
                        }

                        break;
                    }

                }
            }


        }

        // 5. Täienda koodi nii, et farmis saab olla 4 erinevat looma ja igat erinevat looma saab olla 5 tükki
        // 6. Täienda RemoveAnimal nii, et kui sellist looma ei leitud, siis prindib "Looma ei leitud"
        // 7. Täienda GetAnimalCount(string animalType) nii, et kui sellist looma ei leitud, siis prindib "Looma ei leitud"
        // 8. Looge klassid Forest ja Zoo ja mõelge, kuidas teha interface ILivingPlace, kus on kirjas kõik ühine Farm, Zoo ja Forest jaoks

      

        

    }
}
