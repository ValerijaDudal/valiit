﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringFunctions
{
    class Program
    {
        static void Main(string[] args)
        {
            string sentence = "Elas metsas Mutionu";

            // Leia sümbolite arv
            Console.WriteLine("Lauses on {0} sümbolit", sentence.Length);

            // Leia esimese tühiku (või muu sümboli) asukoht (index)
            int spaceIndex = sentence.IndexOf(" ");

            Console.WriteLine("Esimese tühiku indeks on {0}", spaceIndex);

            // Leia esimesest 3 sümbolist koosnev tekstiosa
            string subString = sentence.Substring(0, 3);

            Console.WriteLine("Esimesest 3 sümbolist koosnev tekstiosa on {0}", subString);

            string sentenceWithSpaces = "";
            for (int i = 0; i < sentence.Length; i++)
            {
                sentenceWithSpaces += sentence[i];
                if (i != sentence.Length - 1)
                {
                    sentenceWithSpaces += " ";
                }

            }

            sentenceWithSpaces = "    " + sentenceWithSpaces + "        ";

            Console.WriteLine("\"{0}\"", sentenceWithSpaces);

            Console.WriteLine("\"{0}\"", sentenceWithSpaces.Trim());

            string[] words = new string[] { "Põdral", "maja", "metsa", "sees" };
            string joinedString = string.Join(" ", words);
            Console.WriteLine(joinedString);

            string[] splitWords = joinedString.Split(' ');

            for (int i = 0; i < splitWords.Length; i++)
            {
                Console.WriteLine(splitWords[i]);
            }

            // Küsi kasutajalt nimekiri arvudest nii, et ta eraldab need tühikuga ja liida need kõik arvud kokku

            Console.WriteLine("Sisesta arvude nimekiri, eraldades neid tühikutega");

            string answer = Console.ReadLine();

            // kui Split tükeldab stringi massiiviks, ta ise teab, mitu elemente seal on
            string[] elements = answer.Split(' ');

            int sum = 0;


            for (int i = 0; i < elements.Length; i++)
            {
                Console.WriteLine(elements[i]);

                // Liidame sum'ile iga kord uue elemendi ja saame massiivi elementide summa. Kuna elements oli string, seda liita ei saa, siis konverteerime seda int'iks.
                sum += Convert.ToInt32(elements[i]);
            }

            Console.WriteLine("Summa on {0}", sum);
            // sama
            Console.WriteLine($"Summa on {sum}");



            // Loo numbrite massiiv ja täida see numbritega massiivist elements
            int[] numbers = new int[elements.Length];

            for (int i = 0; i < elements.Length; i++)
            {
                // muudame stringi massiivi int'i massiiviks
                numbers[i] = Convert.ToInt32(elements[i]);
            }

            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }





            // "2 4 6 8" => "2 + 4 + 6 + 8 = 20"

            Console.WriteLine("Prindi ekraanile numbrid 2 4 6 8");

            string answer = Console.ReadLine();

            // Teeme stringist stringi massiivi, eraldades elemendid tühikute kohtades
            string[] splitAnswer = answer.Split(' ');

            for (int i = 0; i < splitAnswer.Length; i++)
            {
                Console.WriteLine(splitAnswer[i]);
            }

            // Teeme stringi massiivist tagasi tavalise stringi, liidades elemendid +'ga
            string joinedBack = string.Join(" + ", splitAnswer);

            // Liidame massiivi elemendid kokku
            int summ = 0;
            for (int i = 0; i < splitAnswer.Length; i++)
            {
                summ += Convert.ToInt32(splitAnswer[i]);
            }

            string result = (joinedBack + " = " + summ);

            Console.WriteLine(result);

            Console.ReadLine();
        }
    }
}
