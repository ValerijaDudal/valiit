﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WriteToFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = new string[] { "Elas metsas Mutionu",
                "keset kuuski noori vanu",
                "kadak põõsa juure all",
                "eluruum tal sügaval" };

            WriteAllLines("MinuFail.txt", lines);


            // Avab streami ja paneb kinni, ei ole vaja eraldi File.Stream'i teha
            // File.WriteAllLines("MinuFail.txt", lines); => 

            // Lisab read massiivist juurde
            File.AppendAllLines("MinuFail.txt", lines);
            // Lisab teksti juurde, mida paneme argumendiks, reavahetused lisame ise
            File.AppendAllText("MinuFail.txt", "Elasmetsas Mutionu\r\nkeset kuuski noori vanu\r\nkadak põõsa juure all\r\neluruum tal sügaval");
        }

        private static void WriteAllLines(string path, string[] lines)
        {
            // Otsib faili MinuFail.txt kaustast, kus asub minu exe fail
            // FileMode.Append kirjutab vanale failile juurde
            // FileMode.Create loob alati uue faili (vana sisu kirjutakse üle)
            // Write => suuname stream'i faili juurde
            FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
            StreamWriter streamWriter = new StreamWriter(fileStream);

            for (int i = 0; i < lines.Length; i++)
            {
                streamWriter.WriteLine(lines[i]);
            }

            streamWriter.Close();
            fileStream.Close();
        }
    }
}
