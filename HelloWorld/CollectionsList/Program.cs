﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Threading.Tasks;


namespace CollectionsList
{
    class Program
    {
        static void Main(string[] args)
        {
            // Massiiv 5 arvust 1, 3, 0, 5, -4
            // Kustuta massiivist esimene number

            int[] numbers = new int[5];

            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }



            // Massiiv 5 sõnast "kala", "auto", "", "maja", "telefon"
            // Kustuta massiivist esimene sõna

            string[] words = new string[] { "kala", "auto", "", "maja", "telefon" };

            // null on tühiühik, tühjus ehk siis mälu pole eraldatud

            words[0] = null;
            words[0] = words[1];
            words[1] = "vesi";

            for (int i = 0; i < words.Length; i++)
            {
                if(words[i] == null)
                {
                    Console.WriteLine("Tühi");
                }
                else
                {
                    Console.WriteLine(words[i]);
                }
            }

            Console.WriteLine();

            
            string word;

            // List kollektsioon
            List<int> numbersList = new List<int>() { 1, 3, 22, -2 };

            // Lisab number 5 lõppu ehk loob 5'le uue elemendi indeksiga 4
            numbersList.Add(5);

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            Console.WriteLine();

            // Lisab uue massiivi lõppu
            numbersList.AddRange(new int[] { 1, 2, 3 });

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            Console.WriteLine();

            // Kustutab elemendi tähendusega 22 või esimest elemendi listis antud tähendusega
            numbersList.Remove(22);

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            Console.WriteLine();

            // Kustutab elemendi indeksiga 2
            numbersList.RemoveAt(2);

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            Console.WriteLine();

            // kustutab alates 2 indeksist järgmised 3 elementi
            numbersList.RemoveRange(2, 3);

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            Console.WriteLine();

            // Lisab esimese indeksi kohale number 1
            numbersList.Insert(1, 1);

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            Console.WriteLine();

            // Lisab 2 elemendi kohale massiivi numbers
            numbersList.InsertRange(2, numbers);

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            Console.WriteLine();

            // Kontrollib, kas listis on element väärtusega 1
            // if(numbersList.IndexOf(1) != -1)
            if(numbersList.Contains(1))
            {
                Console.WriteLine("Nimekirjas on olemas number 1");
            }

            Console.WriteLine();

            // Näitab elemendi indeksit
            Console.WriteLine("Numbri 3 indeks on {0}", numbersList.IndexOf(3));

            // Tõstab elemendid ümber lõpust algusesse
            numbersList.Reverse();

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            Console.WriteLine();

            // Paneb kõik elemendid järjestikku
            numbersList.Sort();

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            // stringi list
            List<string> wordsList = new List<string>();

            // reaalarvude list
            List<double> realNumbers = new List<double>();

            // list erinevatest objektidest string, int, bool, char, double
            ArrayList arrayList = new ArrayList() { 3, "maja", 'c', true, 3.45 };
            arrayList.Add(2);
            arrayList.Add("tere");

            int e = -8;
            string f = "kalamees";
            bool isGood = false;

            arrayList.Insert(1, e);
            arrayList.Insert(1, f);
            arrayList.Insert(1, isGood);

            arrayList.Insert(1, 5);

            for (int i = 0; i < arrayList.Count; i++)
            {
                Console.WriteLine(arrayList[i]);
            }

            Console.WriteLine();

            // is on tüübi kontroll
            // kui element kohal 4 on string, siis on tõene
            if (arrayList[1] is string)
            {
                word = (string)arrayList[1];
                Console.WriteLine(word.ToUpper());
            }

            else if (arrayList[1] is int)
            {
                int g = (int)arrayList[1];
                g++;
                Console.WriteLine(g);
            }

            Console.ReadLine();
   
        }
    }
}
