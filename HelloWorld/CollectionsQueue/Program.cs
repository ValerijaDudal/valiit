﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            // Peeter, Mari, Kaie, Paul
            // anna Peeter (dequeue)
            // Mari, Kaie, Paul
            // lisandub Jüri (enqueue)
            // Mari, Kaie, Paul, Jüri
            // anna Mari
            // Kaie, Paul, Jüri

            Queue<string> queue = new Queue<string>();
            queue.Enqueue("Peeter");
            queue.Enqueue("Mari");
            queue.Enqueue("Kaie");
            queue.Enqueue("Paul");

            // Tsükkel mingi massiivi või muu kollektsiooni läbimiseks
            // Elemente tsükli sees muuta ei saa
            // Iga queues oleva elemendi kohta tee muutuja item ehk iga tsükli korduse sees on string item võrdne järgneva elemendiga
            foreach (string item in queue)
            {
                Console.WriteLine(item);
            }


            // võtame esimese elemendi ära
            string currentClient = queue.Dequeue();

            Console.WriteLine("Teenindatakse klienti {0}", currentClient);

            Console.WriteLine();

            foreach (string item in queue)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            queue.Enqueue("Jüri");

            foreach (string item in queue)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            // Näitab, kes on järgmine klient
            string nextClient = queue.Peek();

            // Kui järgmine klient on Mari, siis võtame ta järjekorrast ära ja lisame lõppu
            if(nextClient == "Mari")
            {
                string name = queue.Dequeue();
                queue.Enqueue(name);
                // või
                //queue.Enqueue(queue.Dequeue());
            }

            foreach (var item in queue)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();

        }
    }
}
