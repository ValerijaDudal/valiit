-- kommentaar
/*
kommentaar nii c# kui ka SQL
*/

SELECT 'Hello World!'; -- Tekst pannakse '' vahele, mitte "" vahele
SELECT 3;
SELECT 3 + 4;
SELECT 3.34 / 4; -- Koma kasutada ei saa
SELECT 'raud' + 'tee'; -- annab s�na raudtee 
SELECT 'Peeter', 'Paat', 23, 23.3456; -- eraldab tabelis veerud

 -- AS saab kasutada veeru pealkirja m��ramiseks
SELECT 'Peeter' AS Eesnimi,
		'Paat' AS Perekonnanimi,
		23 AS Vanus,
		23.3456 AS Kaal,
		'Blond' AS [Juukse v�rv];

K�ik SQL'i k�sud kirjutatakse suurte t�htedega.

-- P�ring tabelist Student, mis tagastab k�ik �pilased ehk siis k�ik veerud ja k�ik read
SELECT * FROM Student;

-- Kui tabelit millegip�rast ei leia, siis proovi Edit -> IntelliSense -> Refresh Local Cache

SELECT FirstName, LastName FROM Student; -- kui ma tahan, et p�ring tagastaks ainult veerud FirstName ja LastName




SELECT
	FirstName AS [First Name],
	'Peeter' AS [Middle Name],
	LastName AS [Last Name],
	FirstName + ' -Peeter ' + LastName AS [Full Name]
FROM
	Student;





SELECT
	FirstName, LastName
FROM
	Student
WHERE
	Age > 25 AND Age < 20 OR Age = 28;

-- WHERE tingimuses =, > v�i < korral ei arvesta ridu, kus on NULL




SELECT
	*
FROM
	Student
WHERE
	Age IS NULL -- IS on nulliga v�rdlemiseks, vanuse veer ei ole t�idetud





SELECT
	*
FROM
	Student
WHERE
	Age IS NOT NULL -- vanus on teada




SELECT
	*
FROM
	Student
WHERE
	Age != 30; -- vanus ei ole 30



SELECT
	*
FROM
	Student
WHERE
	Age IN (22, 34, 35, 40); -- vanus on 22, 34, 35 v�i 40



SELECT
	*
FROM
	Student
WHERE
	Age NOT IN (22, 34, 35, 40);-- vanus ei ole 22, 34, 35 v�i 40



SELECT
	*
FROM
	Student
WHERE
	FirstName IN ('Peeter', 'Ann') AND Age IN (30, 23);



-- Tabeli loomine
CREATE TABLE Loan
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Amount] [real] NOT NULL,
	[Due date] [date] NOT NULL,
	[StudentId] [int] NOT NULL,
	[LoanTypeId] [int] NOT NULL
);


-- Tabelisse lisamine
INSERT INTO Loan
		([Amount], [Due Date], StudentId, LoanTypeId)
VALUES
		(1500, '2020-12-08', 1, 6),
		(1500, '2021-03-10', 2, 5),
		(1500, '2025-04-20', 5, 4),
		(1500, '2030-06-18', 4, 3);


-- Tabeli muutmine
-- UPDATE kunagi ei tohi kasutada ilma WHERE
UPDATE
	Student
SET
	Age = 25
WHERE
	FirstName = 'Anu'
	AND LastName = 'Saar'


-- tabelist kustutamine
-- DELETE kunagi ei tohi kasutada ilma WHERE
DELETE FROM
		Student
WHERE
		Age IS NULL;	


-- Anna mulle k�ik kasutajad koos laenudega
-- INNER JOIN on selline liitmine, kus liidetakse ainult sellised read, kus on seos ON tingimuses ehk ainult need read, kus on t�idetud Student.Id ja Loan.StudentId
SELECT * FROM
		Student
INNER JOIN
		Loan
ON
		Student.Id = Loan.StudentId;


-- Anna mulle k�ik kasutajad, kes on v�lgu
SELECT
		Student.*
FROM
		Student
JOIN
		Loan
ON
		Student.Id = Loan.StudentId;


-- Anna mulle k�ik kasutajad, kes on v�lgu
SELECT
		Student.Id,
		Student.FirstName,
		Student.LastName,
		Loan.Amount,
		Loan.[Due date]
FROM
		Student
JOIN
		Loan
ON
		Student.Id = Loan.StudentId;


-- Anna mulle k�ik kasutajad, kes on v�lgu
SELECT
		Student.Id,
		Student.FirstName,
		Student.LastName,
		Loan.Amount,
		Loan.[Due date],
		LoanType.[Name]
FROM
		Student
JOIN
		Loan
ON
		Student.Id = Loan.StudentId
JOIN
		LoanType
ON
		LoanType.Id = Loan.LoanTypeId


-- INNER JOIN j�rjekord ei ole oluline

-- N�itab k�ik Student
-- Anna mulle k�ik kasutajad, ja n�ita kes on v�lgu ja kes pole
SELECT
		Student.Id,
		Student.FirstName,
		Student.LastName,
		Loan.Amount,
		Loan.[Due date]
FROM
		Student
LEFT JOIN
		Loan
ON
		Student.Id = Loan.StudentId
WHERE
		Loan.Amount > 1300


-- COALESCE vaatab kui esimene parameeter on NULL ja asendab selle teise parameetriga
SELECT
		Student.Id,
		Student.FirstName,
		COALESCE(Student.LastName, Puudub),
		COALESCE(Loan.Amount, 0),
		COALESCE(Loan.[Due date], '2000-01-01')
FROM
		Student
LEFT JOIN
		Loan
ON
		Student.Id = Loan.StudentId


-- k�ik v�imalikud paarid
SELECT
		s.FirstName,
		st.FirstName
FROM
		Student AS s
CROSS JOIN
		Student AS st
WHERE
		s.FirstName != st.FirstName



SELECT
		s.FirstName AS [First Partner],
		st.FirstName AS [Second Partner]
FROM
		Student AS s -- tabelile anda alias
CROSS JOIN
		Student AS st
WHERE
		s.FirstName != st.FirstName



SELECT
		Student.Id,
		Student.FirstName,
		Student.LastName,
		Loan.Amount,
		Loan.[Due date]
FROM
		Student
FULL OUTER JOIN
		Loan
ON
		Student.Id = Loan.StudentId



-- NULL v��rtused on alati esimesed j�rjestamisel
SELECT
		*
FROM
		Student
ORDER BY
		Age



SELECT
		*
FROM
		Student
ORDER BY
		FirstName ASC


-- Ascending on default ja v�ib �ra j�tta
-- NULL v��rtused on alati esimesed j�rjestamisel
SELECT
		*
FROM
		Student
ORDER BY
		LastName, FirstName ASC -- Ascending suurenev



SELECT
		*
FROM
		Student
ORDER BY
		LastName DESC -- Descending



-- Anna mulle k�igi kasutajate perekonnanimed, kes v�tsid smslaenu ja on veel v�lgu �le 100
-- tulemus anna mulle j�rjestatuna vanuse j�rgi vanenemast nooremaks
SELECT
		Student.LastName
FROM
		Student
JOIN
		Loan
ON
		Student.Id = Loan.StudentId
JOIN
		LoanType
ON
		LoanType.Id = Loan.LoanTypeId
WHERE
		LoanTypeId = 1
		AND Loan.Amount > 50
ORDER BY
		Student.Age DESC


-- Anna mulle k�ik kasutajad, kelle eesnime esimesed 2 t�ht on 'an'
SELECT
		*
FROM
		Student
WHERE
		FirstName Like 'An%'


-- Anna k�ik �pilased, kelle perekonnanimi l�ppeb s t�hega
-- Anna k�ik �pilased, kelle perekonnanimi sisaldab 'da'
SELECT
		*
FROM
		Student
WHERE
		LastName Like '%da%'


SELECT
		*
FROM
		Student
WHERE
		LastName Like '%s'



-- n�itab keskmist vanust
SELECT
		AVG(COALESCE(Age, 0))
FROM
		Student



-- COUNT(*) loeab �le k�ik read, COUNT(Age) loeb �le read, aga ainult need, kus see v��rtus ei ole NULL
SELECT
		COUNT (*) / COUNT (Age)
FROM
		Student
FROM
		Student



-- Summa leidmine, maksimum v��rtus, miinimum v��rtus
SELECT
		Sum(Age) / Max(Age) / Min(Age)
FROM
		Student



-- Kui meil on GROUP BY lause, siis tehakse nii�elda vahetulemus ja SELECT lauses ma saan kasutada ainult neid v��rtuseid, mille j�rgi ma grupeerisin (FirstName, LastName)
-- v�i v��rtused kus ma kasutan agregaat funktsioonid
SELECT
		Student.FirstName,
		Student.LastName
FROM
		LoanType
JOIN
		Loan
ON
		LoanType.Id = Loan.LoanTypeId
JOIN
		Student
ON
		Student.Id = Loan.StudentId
GROUP BY
		FirstName, LastName



-- ROUND(summa, mitu-koma-kohta)
SELECT
		Student.FirstName,
		Student.LastName,
		SUM(ROUND(Loan.Amount, 2))
FROM
		LoanType
JOIN
		Loan
ON
		LoanType.Id = Loan.LoanTypeId
JOIN
		Student
ON
		Student.Id = Loan.StudentId
GROUP BY
		FirstName, LastName


-- Laenude arv vanuste kaupa
SELECT
		Student.Age,
		COUNT (COALESCE(Loan.Amount, 0)) AS [Laenude arv]
FROM
		Loan
JOIN
		Student
ON
		Student.Id = Loan.StudentId
GROUP BY
		Student.Age
ORDER BY
		Student.Age ASC



-- N�ita mulle eesnime esimese t�he esinemise statistika
SELECT
		SUBSTRING(FirstName, 1, 1),
		COUNT(FirstName)
FROM
		Student
GROUP BY
		SUBSTRING(FirstName, 1, 1)


-- DISTINCT n�itab k�ik perekonnanimed ilma kordusteta
SELECT
		DISTINCT LastName
FROM
		Student



-- Lisa igale tudengile +1 vanuse juurde
UPDATE
		Student
SET
		Age = Age + 1



-- Anna mulle k�ik keskmise vanusega kasutajad
-- Nested
SELECT
		(SELECT TOP (1) Amount FROM Loan WHERE Id = (SELECT MIN(Id) FROM Loan)), *
FROM
		Student
WHERE
		Age = (SELECT AVG (Age) FROM Student)



-- Anna mulle k�ik kasutajad, kes v�tsid t�pselt k�ikide laenude miinimumi suuruse laenu
SELECT
		*
FROM
		Student
JOIN
	Loan
ON
	Student.Id=Loan.StudentId
WHERE
		Amount = (SELECT MIN (AMOUNT) FROM Loan)



-- Kopeeri andmed �hest tabelist teisse
INSERT INTO Author (FirstName, LastName)
SELECT FirstName, LastName FROM Student



-- Anna mulle k�ik raamatute nimed koos nende autorite nimedega
SELECT 
		Book.[Name], Author.FirstName, Author.LastName
FROM 
		Book
JOIN 
		AuthorBook
ON 
		Book.Id = BookId
JOIN 
		Author
ON 
		Author.Id = AuthorId




-- Anna mulle k�ik autorid koos kirjutatud raamatute arvuga
SELECT 
		Author.FirstName, Author.LastName,
		COUNT (AuthorId) AS [Raamatute arv]
FROM 
		Book
JOIN 
		AuthorBook
ON 
		Book.Id = BookId
JOIN 
		Author
ON 
		Author.Id = AuthorId
GROUP BY
		Author.FirstName, Author.LastName




Package Manager Console:

PM> add-migration AddIsMarried

PM> update-database


