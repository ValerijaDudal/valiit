﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>()
            {
                new Student()
                {
                    FirstName = "Malle",
                    LastName = "Maasikas",
                    Age = 13
                },
                new Student()
                {
                    FirstName = "Peeter",
                    LastName = "Kuusk",
                    Age = 18
                },
                new Student()
                {
                    FirstName = "Karl",
                    LastName = "Mustikas",
                    Age = 17
                },
                new Student()
                {
                    FirstName = "Oliver",
                    LastName = "Mägi",
                    Age = 19
                },
                new Student()
                {
                    FirstName = "Madis",
                    LastName = "Meri",
                    Age = 20
                }

            };



            var oldStudents = (from student in students
                               where student.Age > 18
                               select student).ToList();

            // Sama asi foreach'iga
            var youngStudents = new List<Student>();
            foreach (var student in students)
            {
                if(student.Age < 18)
                {
                    youngStudents.Add(student);
                }
            }

            // Prindi mulle kõikide kasutajate nime initsiaalid

            var initials = (from student in students select student.FirstName[0] + " " + student.LastName[0]
                    ).ToList();

            // Täienda nii, et prindi ainult neid õpilasi kellel nimes on 5 tähte

            var fiveLetters = (from student in students where student.FirstName.Length == 5 select student.FirstName
                               ).ToList();

            // Täisarvude massiivist anna mulle kõik paariarvud
            // a % 2 == 0 (kontrollime jääki)
            int[] numbers = new int[] { 2, 3, 4, 5, 6, 7 };
            var equalNumbers = (from number in numbers where number % 2 == 0 select number).ToList();

            // Anna mulle õpilaste nimikiri, kus perekonnanimes kõik a, e, i on asendatud o-ga
            var changedStudents = (from student in students select student.LastName.Replace('a', 'o').Replace('e', 'o').Replace('i', 'o')).ToList();


            int a = 4;

            int b;
            if(a < 10)
            {
                b = 1;
            }
            else
            {
                b = 2;
            }

            // sama
            b = a < 10 ? 1 : 2;


            Console.ReadLine();
        }
    }
}
