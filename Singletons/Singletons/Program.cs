﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singletons
{
    class Program
    {
       static void Main(string[] args)
        {
            Student firstStudent = new Student() { FirstName = "Malle", LastName = "Maasikas" };
            Student secondStudent = new Student() { FirstName = "Jaan", LastName = "Jalgratas" };

            Console.WriteLine(Student.Count);

            Singleton singleton = Singleton.Instance;
            singleton.Age = 20;

            Singleton secondSingleton = Singleton.Instance;
            Console.WriteLine(secondSingleton.Age);

            Console.ReadLine();
        }
    }
}
