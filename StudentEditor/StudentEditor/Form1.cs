﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace StudentEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Click(object sender, EventArgs e)
        {

        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            string firstName = textBoxFirstName.Text;
            string lastName = textBoxLastName.Text;
            int age = Convert.ToInt32(textBoxAge.Text);

            string connectionString = "Server = DESKTOP-83FDINL; Database = AspNetCore; Trusted_Connection = True";
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            MessageBox.Show(connection.State.ToString());

            string sql =
                "INSERT INTO Student " +
                "(FirstName, LastName, Age) " +
                "VALUES " +
                "(@FirstName, @LastName, @Age)";

            SqlCommand command = new SqlCommand(sql, connection);

            // Kaitse SQL injectioni vastu
            command.Parameters.AddWithValue("@FirstName", firstName);
            command.Parameters.AddWithValue("@LastName", lastName);
            command.Parameters.AddWithValue("@Age", age);

            int linesChanged = command.ExecuteNonQuery();
            MessageBox.Show("Changes count: " + linesChanged);

            connection.Close();
        }
    }
}
