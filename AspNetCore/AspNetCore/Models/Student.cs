﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCore.Models
{
    public class Student
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Eesnimi on kohustuslik")]
        [StringLength(10, MinimumLength = 5)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        [Display(Name = "Last name")]
        public string LastName { get; set; }
        [Range(1, 50, ErrorMessage = "Vanus peab olema 1 ja 50 vahel")]
        public int Age { get; set; }
        [Display(Name = "Is Married")]
        public bool? IsMarried { get; set; }
        [Display(Name = "Is Blind")]
        public bool IsBlind { get; set; }
        public List<Loan> Loan { get; set; }

        [NotMapped]
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }

        public byte[] Picture { get; set; }
    }
}
