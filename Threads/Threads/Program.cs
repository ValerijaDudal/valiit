﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Threads
{
    class Program
    {
        static void Main(string[] args)
        {
            DoWork();
            Console.ReadLine();
        }

        static void DoWork()
        {
            Console.WriteLine("Starting work");
            for (int i = 0; i < 6; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine(i);
            }
            Console.WriteLine("Work done");
        }

    }
}
